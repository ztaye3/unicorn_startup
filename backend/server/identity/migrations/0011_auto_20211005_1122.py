# Generated by Django 3.2.4 on 2021-10-05 11:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('identity', '0010_remove_useraccount_username'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='useraccount',
            name='is_merchant',
        ),
        migrations.DeleteModel(
            name='MerchantModel',
        ),
    ]
