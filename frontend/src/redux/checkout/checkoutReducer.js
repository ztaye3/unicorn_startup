import {
    CALL_CHECKOUT_FAIL,
    CALL_CHECKOUT_SUCCESS,
    STRIP_CHECKOUT_BACKEND_FAIL,
    STRIP_CHECKOUT_BACKEND_SUCCESS,
    STRIP_CHECKOUT_FRONTEND_FAIL,
    STRIP_CHECKOUT_FRONTEND_SUCCESS,
    UPDATE_ACTIVE_STATE_FAIL,
    UPDATE_ACTIVE_STATE_SUCCESS,
    UPDATE_PAYMENT_DETAILS_SUCCESS,
    UPDATE_SHIPPING_SUCCESS,
    SET_SELL_NFT_SUCCESS,
    SET_SELL_NFT_FAIL,
    SET_SELL_FIXED_PRICE_NFT_SUCCESS,
    RESET_ACTIVE_STEP_SUCCESS

} from "./type";
import {isEmptyUtils} from "../../utils/Utils";



const initialState = {
   order: {},
   checkoutData : {},
   amountBought: {},
   isFrontendCheckout: false,
   isBackendCheckout: false,
    shippingData: {
                    minimum_bid: '',
                    reserved_price: '',
                    expiration_date: ''
    },
  fixed_price: ''
    ,
  payment: {},
  activeStep: 0,
  sell_nft_id: []

}

const checkoutReducer = (state =initialState, action) => {
    switch (action.type){

        case CALL_CHECKOUT_SUCCESS:
            return {
                ...state,
                amountBought: action.payload,
                isAddToCart: true
            }
        case CALL_CHECKOUT_FAIL:
            return {
                ...state,
                isAddToCart: false
            }
        case STRIP_CHECKOUT_FRONTEND_SUCCESS:
            return {
                ...state,
                isFrontendCheckout: true,
                order: action.payload,
                activeStep: 0
            }
        case STRIP_CHECKOUT_FRONTEND_FAIL:
            return {
                ...state,
                isFrontendCheckout: false
            }
        case STRIP_CHECKOUT_BACKEND_SUCCESS:
            return {
                ...state,
                isBackendCheckout: true
            }
        case STRIP_CHECKOUT_BACKEND_FAIL:
            return {
                ...state,
                isBackendCheckout: false
            }

        case UPDATE_SHIPPING_SUCCESS:
            return {
                ...state,
                shippingData: action.payload
            }
        case UPDATE_PAYMENT_DETAILS_SUCCESS:
            return {
                ...state,
                payment: action.payload
            }
        case UPDATE_ACTIVE_STATE_SUCCESS:
            return {
                ...state,
                activeStep: action.payload
            }
        case UPDATE_ACTIVE_STATE_FAIL:
            case SET_SELL_NFT_SUCCESS:
            return {
                ...state,
                sell_nft_id: action.payload
            }
        case RESET_ACTIVE_STEP_SUCCESS:
            return {
                ...state,
                activeStep: 0
            }
        case SET_SELL_NFT_FAIL:
            case SET_SELL_FIXED_PRICE_NFT_SUCCESS:
            return {
                ...state,
                fixed_price: action.payload
            }
            default:
            return state
    }
}

export default checkoutReducer;