import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import i18next from "i18next";
import {useTranslation, withTranslation} from "react-i18next";
import { Translation } from 'react-i18next';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import productAction from "../../../redux/admin/product/productAction";
import cartAction from "../../../redux/cart/cartAction";
import checkoutAction from "../../../redux/checkout/checkoutAction";
import {withRouter} from "react-router-dom";
import {CHECKOUT_URL, UPDATE_SHIPPING} from "../../../utils/Constant";

class Biding extends Component  {

    constructor(props) {
        super(props);
        this.state = {
            minimum_bid: this.props.shipping.minimum_bid,
            reserved_price: this.props.shipping.reserved_price,
            expiration_date: this.props.shipping.expiration_date,
        }
    }

    componentWillUnmount () {
        this.props.checkoutAction(this.state, UPDATE_SHIPPING, CHECKOUT_URL)
    }

    onChange = e => {

        e.preventDefault();
        const target = e.target;
        const  value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value    });

    }

    render() {
      // Translation object
      const { t } = this.props;

      const translate = (key) => {
        return (
            <Translation>
                {
                    (t, {i18n}) => <Typography>{t(key)}</Typography>
                }
            </Translation>
    )
}

      return (

                <React.Fragment>
                  <Typography variant="h6" gutterBottom>
                    {t("checkout_bid_description")}
                  </Typography>
                  <Grid container spacing={3}>

                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="minimum_bid"
                        name="minimum_bid"
                        label={t("checkout_minimum_bid")}
                        fullWidth
                        autoComplete="minimum_bid"
                        onChange={this.onChange}
                        value={this.state.minimum_bid}
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="reserved_price"
                        name="reserved_price"
                        label={t("checkout_reserved_price")}
                        fullWidth
                        autoComplete="reserved_price"
                        onChange={this.onChange}
                        value={this.state.reserved_price}
                      />
                    </Grid>


                    <Grid item xs={12}>

                              <TextField
                                id="expiration_date"
                                name="expiration_date"
                                label="Next appointment"
                                type="datetime-local"
                                defaultValue={this.state.expiration_date}
                                sx={{ width: 250 }}
                                onChange={this.onChange}
                                InputLabelProps={{
                                  shrink: true,
                                }}
                              />
                    </Grid>

                  </Grid>
                </React.Fragment>

          );
  }
}

Biding.propTypes = {
  productAction: PropTypes.func.isRequired,
  loginUser: PropTypes.object.isRequired,
    products: PropTypes.object.isRequired,
  cartAction: PropTypes.func.isRequired,
  cart: PropTypes.object.isRequired,
    checkoutAction: PropTypes.func.isRequired,
    checkout: PropTypes.object.isRequired,
    shipping: PropTypes.object.isRequired,

};

const mapStateToProps = state => {
    return {
        loginUser: state.loginUser,
        products: state.productAdmin.products,
        cart: state.cart.cart,
        checkout: state.checkout,
        shipping: state.checkout.shippingData
    }
}
export default connect(mapStateToProps, {productAction, cartAction, checkoutAction})(withTranslation() (withRouter(Biding)));
