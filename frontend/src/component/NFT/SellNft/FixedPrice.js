import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import i18next from "i18next";
import {useTranslation, withTranslation} from "react-i18next";
import { Translation } from 'react-i18next';
import PropTypes from "prop-types";
import {connect} from "react-redux";
import productAction from "../../../redux/admin/product/productAction";
import cartAction from "../../../redux/cart/cartAction";
import checkoutAction from "../../../redux/checkout/checkoutAction";
import {withRouter} from "react-router-dom";
import {
    CHECKOUT_URL,
    SELL_NFT_CHECKOUT_FRONTEND_URL,
    SET_FIXED_PRICE_NFT,
    UPDATE_SHIPPING
} from "../../../utils/Constant";

class FixedPrice extends Component  {

    constructor(props) {
        super(props);
        this.state = {
            fixed_price: this.props.fixed_price,
        }
    }

    componentWillUnmount () {
        this.props.checkoutAction(this.state.fixed_price, SET_FIXED_PRICE_NFT, SELL_NFT_CHECKOUT_FRONTEND_URL)
    }

    onChange = e => {

        e.preventDefault();
        const target = e.target;
        const  value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
          [name]: value    });

    }

    render() {
      // Translation object
      const { t } = this.props;

      const translate = (key) => {
        return (
            <Translation>
                {
                    (t, {i18n}) => <Typography>{t(key)}</Typography>
                }
            </Translation>
    )
}

      return (

                <React.Fragment>
                  <Typography variant="h6" gutterBottom>
                    {t("checkout_fixed_description")}
                  </Typography>
                  <Grid container spacing={3}>
                     <Grid item xs={12}>
                      <TextField
                        required
                        id="fixed_price"
                        name="fixed_price"
                        label={t("checkout_fixed_price")}
                        fullWidth
                        autoComplete="fixed price"
                        onChange={this.onChange}
                        value={this.state.fixed_price}
                      />
                    </Grid>

                  </Grid>
                </React.Fragment>

          );
  }
}

FixedPrice.propTypes = {
  productAction: PropTypes.func.isRequired,
  loginUser: PropTypes.object.isRequired,
    products: PropTypes.object.isRequired,
  cartAction: PropTypes.func.isRequired,
  cart: PropTypes.object.isRequired,
    checkoutAction: PropTypes.func.isRequired,
    checkout: PropTypes.object.isRequired,
    fixed_price: PropTypes.object.isRequired,

};

const mapStateToProps = state => {
    return {
        loginUser: state.loginUser,
        products: state.productAdmin.products,
        cart: state.cart.cart,
        checkout: state.checkout,
        fixed_price: state.checkout.fixed_price
    }
}
export default connect(mapStateToProps, {productAction, cartAction, checkoutAction})(withTranslation() (withRouter(FixedPrice)));
