from rest_framework.decorators import api_view
from rest_framework.views import APIView
from .models import OrderModel
from .serializers import OrderSerializerRead, OrderSerializerWrite, ShippingSerializer, OwnerSerializer
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
# from identity.models import *
from django.http import JsonResponse
import json
from .models import OrderModel
import stripe
from product.models import ProductModel, OwnerModel

stripe.api_key = "sk_test_51JIETXFjmXVud4GbTwREo6EqrrUz43tstEMnEvrBniCi5hVqdPclj2oRICqkcj2tsgvTxTijy79kdeKaFGIL7MIM00SlxMb5En"
from .enums import OrderStatusEnum
from django.core.mail import send_mail
from identity.models import UserAccount
import requests
import time


@api_view(['POST'])
def test_payment(request):
    test_payment_intent = stripe.PaymentIntent.create(
        amount=1000, currency='CHF',
        payment_method_types=['card'],
        receipt_email='test@example.com')
    return Response(status=status.HTTP_200_OK, data=test_payment_intent)


# Order view
class OrderView(viewsets.ViewSet):
    # permission_classes = [IsAuthenticated]
    queryset = OrderModel.objects.all()

    # Get all order API : api/order/v1/, Method:GET
    def list(self, request, page=None, offset=None):

        # Check if any order exist
        try:
            queryset = OrderModel.objects.all()
        except OrderModel.DoesNotExist:
            return Response(data={'message': 'No order registered'}, status=status.HTTP_204_NO_CONTENT)
        else:

            serializer = OrderSerializerRead(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

    # Create order API : order/api/v1/, Method:POST
    def create(self, request):

        # filter data
        data = request.data

        is_sell_NFT = data[0]

        payment = data[1]

        order = data[2]

        products = data[3]

        # Stripe payment
        stripe_payment(payment)

        serializer = OrderSerializerWrite(data=order)

        if serializer.is_valid():

            # Save order
            serializer.save()

            # Add orders
            order_id = serializer.data.get('id')
            order = OrderModel.objects.get(id=order_id)

            # Update order status
            OrderModel.objects.filter(pk=order_id).update(order_status=OrderStatusEnum.PAYMENT_SUCCESSFUL)

            # check for sell NFT
            if is_sell_NFT:
                # Add products to order
                product = ProductModel.objects.get(id=products['id'])

                # update product
                if products["is_bid"]:
                    product.min_bid_price = float(products["min_bid_price"])

                product.market_price = float(products["market_price"])
                product.expiration_date = products["expiration_date"]
                product.is_bid = products["is_bid"]

                product.save()

                # claim nft token
                mint_data = {'from': payment['email']}
                call_mint(mint_data)

                # save product
                order.products.add(product)
            else:
                # Add products to order
                buyer = payment['email']

                for code, value in products["products"].items():

                    # update order
                    for seller, unit in value.items():
                        print("Unit")
                        print(unit)

                        order_update = OrderModel.objects.get(id=seller)
                        order_update.is_NFT_sold = True
                        order_update.save()
                        seller_email = order_update.email

                        # add product to order
                        product = ProductModel.objects.get(code=code)
                        order.products.add(product)

                        # update owners
                        for owners in product.owners.all():
                            # update seller nft
                            if owners.email == seller_email:
                                queryset = OwnerModel.objects.get(id=owners.id)
                                queryset.nft_number = queryset.nft_number - unit
                                queryset.save()

                            # update buyer nft
                            if owners.email == buyer:
                                # if buyer already bought before
                                queryset = OwnerModel.objects.get(id=owners.id)
                                queryset.nft_number = queryset.nft_number + unit
                                queryset.save()

                            else:
                                # if owner is new, add
                                owner_data = {
                                    "email": buyer,
                                    "nft_number": unit
                                }

                                # add owner
                                id = add_owner(owner_data)

                                get_owner = OwnerModel.objects.get(id=id)
                                product.owners.add(get_owner)

                                # register blockchain transaction

                                chain_data = {
                                    "fee": 10,
                                    "transaction": unit,
                                    "from": buyer,
                                    "from_id": id,
                                    "id": product.id
                                }

                                call_mutate(chain_data)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # Get order API : api/order/v1/'pk', Method:GET
    def retrieve(self, request, pk=None):
        # Check if the order is exist

        try:
            queryset = OrderModel.objects.get(id=pk)
        except OrderModel.DoesNotExist:
            return Response(data={'message': 'Order does not exits'}, status=status.HTTP_204_NO_CONTENT)

        else:
            serializer = OrderSerializerRead(queryset, many=False)
            return Response(serializer.data, status=status.HTTP_200_OK)

    # Update order API : api/order/v1/'pk', Method:PUT
    def update(self, request, pk=None):

        # Check if the product exists
        try:

            data = request.data
            # check if it's update bid
            is_update_bid = data.get('is_update_bid')
            order_id = data.get('id')

            if is_update_bid is not None:
                queryset = OrderModel.objects.get(id=order_id)
                queryset.recent_given_bid = data['recent_given_bid']
                queryset.save()

        except OrderModel.DoesNotExist:
            return Response(data={'message': 'Order does not exits'}, status=status.HTTP_204_NO_CONTENT)
        else:
            serializer = OrderSerializerWrite(instance=queryset, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    # Delete order API : api/order/v1/'pk', Method:DELETE
    def destroy(self, request, pk=None):
        # Check if the order exists
        try:
            queryset = OrderModel.objects.get(id=pk)
        except OrderModel.DoesNotExist:
            return Response(data={'message': 'OrderModel does not exits'}, status=status.HTTP_204_NO_CONTENT)
        else:
            queryset.delete()
            return Response(status=status.HTTP_200_OK)


# order confirmation email sender
def send_order_confirmation(email, order_id):
    # Send mail
    send_mail(
        'NFTify order confirmation',
        'Your order number is ' + str(order_id) + ' . We will send you an update when your order has shipped ',
        email,
        [email],
        fail_silently=False,
    )


# stripe payment execution
def stripe_payment(data):
    email = data['email']
    payment_method_id = data['id']
    amount = data['amount']
    currency = data['currency']
    payment_method = data["payment_method"]

    extra_msg = ''  # add new variable to response message
    # checking if customer with provided email already exists
    customer_data = stripe.Customer.list(email=email).data

    # if the array is empty it means the email has not been used yet
    if len(customer_data) == 0:
        # creating customer
        customer = stripe.Customer.create(
            email=email, payment_method=payment_method_id)
    else:
        customer = customer_data[0]
        extra_msg = "Customer already existed."

    # Stripe payment
    stripe.PaymentIntent.create(
        customer=customer,
        payment_method_types=payment_method,
        payment_method=payment_method_id,
        currency=currency,
        amount=amount,
        confirm=True)


def add_shipping(order, shipping):
    serializer = ShippingSerializer(data=shipping)

    if serializer.is_valid():
        # Save shipping
        serializer.save()
        shipping_id = serializer.data.get('id')
        order.shipping.add(shipping_id)


# owner creator
def add_owner(owner):
    serializer = OwnerSerializer(data=owner)
    serializer.is_valid(raise_exception=True)
    serializer.save()

    return serializer.data['id']


# call mint API
def call_mint(input):
    """
     Create nft token
     http://localhost:1317/assets/mint
     :param input: JSON
     :return: Boolean - Status of transaction
     """

    data = {
        "type": "string",
        "value": {
            "baseReq": {
                "account_number": 0,
                "chain_id": "test",
                "fees": [
                    {
                        "amount": {'transaction_fee': 10},
                        "denom": "string"
                    }
                ],
                "from": input['from'],
                "gas": "string",
                "gas_adjustment": "string",
                "memo": "string",
                "sequence": 0,
                "simulate": True
            },
            "classificationID": "string",
            "fromID": input['from'],
            "immutableMetaProperties": "string",
            "immutableProperties": "string",
            "mutableMetaProperties": "string",
            "mutableProperties": "string",
            "toID": input['from']
        }
    }

    attempt_num = 0  # keep track of how many times we've retried
    try:
        while attempt_num < 3:
            url = 'http://localhost:1317/assets/mint'
            payload = {data}
            r = requests.post(url, data=payload)
            if r.status_code == 200:
                data = r.json()
                return Response(data, status=status.HTTP_200_OK)
            else:
                attempt_num += 1
                # You can probably use a logger to log the error here
                time.sleep(5)  # Wait for 5 seconds before re-trying
        return Response({"error": "Request failed"}, status=r.status_code)
    except Exception as e:
        return


# call mutate API
def call_mutate(input):
    """
     Create nft token
     http://localhost:1317/assets/mutate
     :param input: JSON
     :return: Boolean - Status of transaction
     """

    data = {
        "type": "string",
        "value": {
            "baseReq": {
                "account_number": 0,
                "chain_id": "test",
                "fees": [
                    {
                        "amount": {"fee": input['fee'], "transaction": input['transaction']},
                        "denom": "string"
                    }
                ],
                "from": input["from"],
                "gas": "string",
                "gas_adjustment": "string",
                "memo": "string",
                "sequence": 0,
                "simulate": True
            },
            "assetID": input['id'],
            "fromID": input['from_id'],
            "mutableMetaProperties": "string",
            "mutableProperties": "string"
        }
    }

    attempt_num = 0  # keep track of how many times we've retried
    try:
        while attempt_num < 3:
            url = 'http://localhost:1317/assets/mutate'

            payload = {data}
            r = requests.post(url, data=payload)
            if r.status_code == 200:
                data = r.json()
                return Response(data, status=status.HTTP_200_OK)
            else:
                attempt_num += 1
                # You can probably use a logger to log the error here
                time.sleep(5)  # Wait for 5 seconds before re-trying
        return Response({"error": "Request failed"}, status=r.status_code)
    except Exception as e:
        return
