from djoser.serializers import UserCreateSerializer
from django.contrib.auth import get_user_model
from rest_framework.serializers import ModelSerializer

# Get user model
User = get_user_model()


# User serializer
class UserCreateSerializer(UserCreateSerializer):
    class Meta(UserCreateSerializer.Meta):
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'password',
                  'profile_picture', 'is_customer', 'is_staff', 'is_active', 'is_activated',
                  'is_admin', 'updated_by', 'created_by'
                  )


# User serializer
class UserAdminSerializer(ModelSerializer):
    class Meta(UserCreateSerializer.Meta):
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'is_admin',
                  'is_customer', 'is_staff', 'is_active', 'is_activated', 'profile_picture'
                  )


# User serializer
class UserAdminUpdateSerializer(ModelSerializer):
    class Meta(UserCreateSerializer.Meta):
        model = User
        fields = ('id', 'email', 'first_name', 'last_name', 'is_admin',
                  'is_customer', 'is_staff', 'is_active', 'is_activated'
                  )
        depth = 1


# Update profile serializer
class UpdateUserProfileSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'is_customer', 'profile_picture', 'is_admin')


