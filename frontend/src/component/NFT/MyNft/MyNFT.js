import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Translation, withTranslation} from "react-i18next";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import {Link as RouterLink, withRouter} from "react-router-dom";
import Container from "@material-ui/core/Container";
import {connect} from "react-redux";
import productAction from "../../../redux/admin/product/productAction";
import {withStyles} from "@material-ui/core/styles";
import Content from "../../../Dashboard/Content";
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import { BiDrink } from 'react-icons/bi';
import { GiTravelDress } from 'react-icons/gi';
import KitchenIcon from '@material-ui/icons/Kitchen';
import LocalLaundryServiceIcon from '@material-ui/icons/LocalLaundryService';
import categoryAction from "../../../redux/product/category/categoryAction";
import {
    ADD_TO_CART, CART_URL,
    CATEGORY_URL, DELETE_SLIDER, GET_ALL_MY_NFT,
    GET_PRODUCT_BY_CATEGORY,
    GET_PRODUCT_BY_NAME, GET_PRODUCT_TYPE, HOME_URL, MANAGE_PRODUCT_TYPE_URL, MANAGE_SLIDER_URL, PRODUCT_CATEGORY_FOUR,
    PRODUCT_CATEGORY_ONE, PRODUCT_CATEGORY_THREE,
    PRODUCT_CATEGORY_TWO, SEARCH_BY_CATEGORY_DEFAULT, UPDATE_RATE
} from "../../../utils/Constant";
import Link from "@material-ui/core/Link";
import cartAction from "../../../redux/cart/cartAction";
import FormControl from "@material-ui/core/FormControl";
import {InputLabel, NativeSelect} from "@material-ui/core";
import FormHelperText from "@material-ui/core/FormHelperText";
import Footer from "../../../utils/Footer";


const styles = theme => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
    paddingLeft: theme.spacing(0),
    paddingRight: theme.spacing(0)
  },
  card: {
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
          paddingTop: theme.spacing(30),
    paddingBottom: theme.spacing(0),
  },
});

class MyNFT extends Component {

    constructor(props) {
        super(props);

    }



    componentDidMount() {

        const input = {
            "owner": this.props.loginUser.user.email
        }

        this.props.productAction(input, GET_ALL_MY_NFT, "/myNFT")


    }


    removeProduct = id => {
        let data = {
            'id': id
        }
       this.props.productAction(data, "deleteProduct", "/myNFT")
        const input = {
            "owner": this.props.loginUser.user.email
        }

        this.props.productAction(input, GET_ALL_MY_NFT, "/myNFT")
    }
    render() {

        const { classes } = this.props;
        const { t } = this.props;
        let products = this.props.products;


        const translate = (key) => {
            return (
                <Translation>
                    {
                        (t, {i18n}) => <Typography>{t(key)}</Typography>
                    }
                </Translation>
            )
    }



        // Define host
        let host = window.location.hostname;

        // Check dev and production host
        if(host === "localhost" || host === "0.0.0.0"){
            host = "http://" + host;
        }

        else {
            host = "https://" + host;
        }

        function Copyright() {
          return (
            <Typography variant="body2" color="textSecondary" align="center">
              {"Copyright © "}
              <Link color="inherit" href="https://klar.zekariashirpo.com/">
                Klar Inc
              </Link>{" "}
              {new Date().getFullYear()}
              {"."}
            </Typography>
          );
        }

        return (
            <div>
                <Content>
                    <Container className={classes.cardGrid} maxWidth="xl">
                          <Grid container spacing={5}>
                            {products.map((product, index) => (
                              <Grid item key={product.name} xs={12} sm={6} md={4}>
                                <Card className={classes.card}>
                                  <CardMedia
                                    className={classes.cardMedia}
                                    image={host + product.image}
                                    title= {product.description}
                                  />
                                  <CardContent className={classes.cardContent}>
                                    <Grid container wrap="nowrap" >
                                         <Grid item xs>
                                           <Typography gutterBottom variant="h5" component="h2">
                                            {product.name}
                                         </Typography>
                                        </Grid>
                                    </Grid>

                                  </CardContent>
                                  <CardActions>
                                    <Button
                                      color="secondary"
                                      variant="outlined"
                                      component={RouterLink}
                                      onClick={()=>this.removeProduct(product.id)}
                                      fullWidth
                                    >
                                      <Typography>{t('cart_button_remove')}</Typography>
                                    </Button>
                                  </CardActions>
                                </Card>
                              </Grid>
                            ))}
                          </Grid>
                    </Container>
                    <footer className={classes.footer}>
                        <Footer/>
                      </footer>
                </Content>
            </div>
        );
    }
}

MyNFT.propTypes = {
  categoryAction: PropTypes.func.isRequired,
  loginUser: PropTypes.object.isRequired,
  cartAction: PropTypes.func.isRequired,
    productAction: PropTypes.func.isRequired,
  cart: PropTypes.object.isRequired,
    productType: PropTypes.object.isRequired,
    productCategory: PropTypes.object.isRequired,
    products: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
    return {
        loginUser: state.loginUser,
        productCategory: state.productCategory,
        cart: state.cart.cart,
        productType: state.productAdmin.productType,
        products: state.productAdmin.nft_products,
    }
}


export default connect(mapStateToProps, {categoryAction, cartAction, productAction})(withTranslation()(withStyles(styles) (withRouter(MyNFT))));

