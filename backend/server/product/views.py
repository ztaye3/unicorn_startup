from rest_framework.views import APIView
from .models import ProductModel, ProductTypeModel, ImageSliderModel, OwnerModel
from .serializers import ProductSerializer, ProductAdminSerializer, ProductTypeSerializer, ImageSliderSerializer, \
    OwnerSerializer
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from identity.models import UserAccount
from django.http import JsonResponse
import json
from django.core.paginator import Paginator
import string
import random


# Product code generator
def code_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# Product view
class ProductView(viewsets.ViewSet):
    # permission_classes = [IsAuthenticated]
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer

    # Get all product API : api/product/v1/pn/offset, Method:GET
    def list(self, request, page=None, offset=None):

        # Check if any product exist
        try:
            queryset = ProductModel.objects.all()
        except ProductModel.DoesNotExist:
            return Response(data={'message': 'No product registered'}, status=status.HTTP_204_NO_CONTENT)
        else:

            serializer = ProductAdminSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

    # Create product API : product/api/v1/, Method:POST
    def create(self, request):

        # filter data
        data = request.data

        type = data['product_type']

        owner = data['owner']

        # get current locale
        locale = data['locale']

        # Generate product code
        code = code_generator()

        # Check if the code is already exist
        product_check = ProductModel.objects.filter(code=code)

        # Keep generating new code if it already exists

        while product_check.exists():
            code = code_generator()
            product_check = ProductModel.objects.filter(code=code)

        # Append 'code' to 'product'
        data['code'] = code

        # Remove M-N relationship key
        data.pop('product_type', None)
        data.pop('product_unit', None)
        data.pop('locale', None)
        data.pop('owner', None)

        # Todo:  Estimate rate

        serializer = ProductSerializer(data=data)

        if serializer.is_valid():

            # Save product
            serializer.save()

            product_model = ProductModel.objects.get(code=code)

            try:
                get_type = ProductTypeModel.objects.get(**{locale: type})
            except ProductTypeModel.DoesNotExist:
                return Response(data={'message': 'Product unit does not exits'}, status=status.HTTP_204_NO_CONTENT)

            try:
                get_owner = UserAccount.objects.get(email=owner)
            except UserAccount.DoesNotExist:
                return Response(data={'message': 'Owner does not exits'}, status=status.HTTP_204_NO_CONTENT)

            # Add type
            add_product_type(product_model, get_type)

            # Add owner
            add_product_owner(product_model, get_owner, owner)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # Get product API : api/product/v1/'pk', Method:GET
    def retrieve(self, request, pk=None):
        # Check if the product is exist

        try:
            queryset = ProductModel.objects.get(id=pk)
        except ProductModel.DoesNotExist:
            return Response(data={'message': 'Product does not exits'}, status=status.HTTP_204_NO_CONTENT)

        else:
            serializer = ProductSerializer(queryset, many=False)
            return Response(serializer.data, status=status.HTTP_200_OK)

    # Update product API : api/product/v1/'pk', Method:PUT
    def update(self, request, pk=None):

        # Check if the product exists
        try:
            queryset = ProductModel.objects.get(id=pk)
        except ProductModel.DoesNotExist:
            return Response(data={'message': 'Product does not exits'}, status=status.HTTP_204_NO_CONTENT)
        else:
            serializer = ProductSerializer(instance=queryset, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    # Delete product API : api/product/v1/'pk', Method:DELETE
    def destroy(self, request, pk=None):
        # Check if the product exists
        try:
            queryset = ProductModel.objects.get(id=pk)
        except ProductModel.DoesNotExist:
            return Response(data={'message': 'ProductModel does not exits'}, status=status.HTTP_204_NO_CONTENT)
        else:
            queryset.delete()
            return Response(status=status.HTTP_200_OK)


# Filter Products by name
class SearchProductByName(ListAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer

    # Search product API : api/product/v1/searchProduct, Method:POST
    def post(self, request, *args, **kwargs):
        product_name = request.data['name']
        locale = request.data['locale']

        # Check if any product exist
        try:
            queryset = ProductModel.objects.filter(name__startswith=product_name).filter(market_price__gte=1.0)
        except ProductModel.DoesNotExist:
            return Response(data={'message': 'No product registered'}, status=status.HTTP_204_NO_CONTENT)
        else:

            serializer = ProductSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)


# Filter Products by category
class SearchProductByCategory(ListAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer

    # Search product API : api/product/v1/searchCategory, Method:POST
    def post(self, request, *args, **kwargs):
        product_category = request.data['category']
        locale = request.data['locale']

        # Check if any product exist
        try:
            product_category = ProductTypeModel.objects.filter(**{locale: product_category})[0]
            queryset = ProductModel.objects.filter(product_type=product_category).filter(market_price__gte=1.0)
        except ProductModel.DoesNotExist:
            return Response(data={'message': 'No product registered'}, status=status.HTTP_204_NO_CONTENT)
        else:

            serializer = ProductSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)


# Filter Products by rate
class SearchProductByRate(ListAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer

    # Search product API : api/product/v1/searchByRate, Method:POST
    def post(self, request, *args, **kwargs):
        product_rate = request.data['rate']
        offset = request.data['offset']
        page = request.data['page']

        # Check if any product exist
        try:
            queryset = ProductModel.objects.filter(rating_sum__gte=product_rate)
        except ProductModel.DoesNotExist:
            return Response(data={'message': 'No product registered'}, status=status.HTTP_204_NO_CONTENT)
        else:

            # Declare paginator, offset = number of content per page
            paginator = Paginator(queryset, offset)
            page = paginator.page(page)
            serializer = ProductSerializer(page, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)


# Update Product rate
class UpdateProductRate(ListAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer

    # Update product API : api/product/v1/updateRate, Method:POST
    def post(self, request, *args, **kwargs):
        product_rate = request.data['rate']
        product_code = request.data['code']

        # Check if any product exist
        try:
            queryset = ProductModel.objects.get(code=product_code)
        except ProductModel.DoesNotExist:
            return Response(data={'message': 'No product registered'}, status=status.HTTP_204_NO_CONTENT)
        else:

            # Update rate of the product = (rating_sum + rate) / (1 + raters_sum)

            raters_sum = queryset.raters_sum + 1
            rating_sum = (float(queryset.rating_sum) + float(product_rate)) / float(raters_sum)

            # Update values
            queryset.raters_sum = raters_sum
            queryset.rating_sum = rating_sum
            queryset.save()

            serializer = ProductSerializer(queryset, many=False)

            return Response(serializer.data, status=status.HTTP_200_OK)


# Product type view
class ProductTypeView(viewsets.ViewSet):
    # permission_classes = [IsAuthenticated]
    queryset = ProductTypeModel.objects.all()
    serializer_class = ProductTypeSerializer

    # Get all product API : api/product/v1/type/pn/offset, Method:GET
    def list(self, request, page=None, offset=None):

        # Check if any product exist
        try:
            queryset = ProductTypeModel.objects.all()
        except ProductTypeModel.DoesNotExist:
            return Response(data={'message': 'No product type registered'}, status=status.HTTP_204_NO_CONTENT)
        else:

            serializer = ProductTypeSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

    # Create product API: product / api / v1 /type, Method: POST
    def create(self, request):

        serializer = ProductTypeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    # Get product API : api/product/v1/type/'pk', Method:GET
    def retrieve(self, request, pk=None):
        # Check if the product is exist

        try:
            queryset = ProductTypeModel.objects.get(id=pk)
        except ProductTypeModel.DoesNotExist:
            return Response(data={'message': 'Product type does not exits'}, status=status.HTTP_204_NO_CONTENT)

        else:
            serializer = ProductTypeSerializer(queryset, many=False)
            return Response(serializer.data, status=status.HTTP_200_OK)

    # Update product API : api/product/v1/type/'pk', Method:PUT
    def update(self, request, pk=None):

        # Check if the product exists
        try:
            queryset = ProductTypeModel.objects.get(id=pk)
        except ProductTypeModel.DoesNotExist:
            return Response(data={'message': 'Product type does not exits'}, status=status.HTTP_204_NO_CONTENT)
        else:
            serializer = ProductTypeSerializer(instance=queryset, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    # Delete product API : api/product/v1/type/'pk', Method:DELETE
    def destroy(self, request, pk=None):
        # Check if the product exists
        try:
            queryset = ProductTypeModel.objects.get(id=pk)
        except ProductTypeModel.DoesNotExist:
            return Response(data={'message': 'Product type does not exits'}, status=status.HTTP_204_NO_CONTENT)
        else:
            queryset.delete()
            return Response(status=status.HTTP_200_OK)


# Product name view
class ImageSliderView(viewsets.ViewSet):
    # permission_classes = [IsAuthenticated]
    queryset = ImageSliderModel.objects.all()
    serializer_class = ImageSliderSerializer

    # Get all product API : api/product/v1/type/pn/offset, Method:GET
    def list(self, request, page=None, offset=None):

        # Check if any product exist
        try:
            queryset = ImageSliderModel.objects.all()
        except ImageSliderModel.DoesNotExist:
            return Response(data={'message': 'No image registered'}, status=status.HTTP_204_NO_CONTENT)
        else:

            serializer = ImageSliderSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

    # Create product API: product / api / v1 /type, Method: POST
    def create(self, request):

        serializer = ImageSliderSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    # Get product API : api/product/v1/type/'pk', Method:GET
    def retrieve(self, request, pk=None):
        # Check if the product is exist

        try:
            queryset = ImageSliderModel.objects.get(id=pk)
        except ImageSliderModel.DoesNotExist:
            return Response(data={'message': 'image does not exits'}, status=status.HTTP_204_NO_CONTENT)

        else:
            serializer = ImageSliderSerializer(queryset, many=False)
            return Response(serializer.data, status=status.HTTP_200_OK)

    # Update product API : api/product/v1/type/'pk', Method:PUT
    def update(self, request, pk=None):

        # Check if the product exists
        try:
            queryset = ImageSliderModel.objects.get(id=pk)
        except ImageSliderModel.DoesNotExist:
            return Response(data={'message': 'Image does not exits'}, status=status.HTTP_204_NO_CONTENT)
        else:
            serializer = ImageSliderSerializer(instance=queryset, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

    # Delete product API : api/product/v1/type/'pk', Method:DELETE
    def destroy(self, request, pk=None):
        # Check if the product exists
        try:
            queryset = ImageSliderModel.objects.get(id=pk)
        except ImageSliderModel.DoesNotExist:
            return Response(data={'message': 'Image does not exits'}, status=status.HTTP_204_NO_CONTENT)
        else:
            queryset.delete()
            return Response(status=status.HTTP_200_OK)


# Filter Products by rate
class MyNFT(ListAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer

    # Search product API : api/product/v1/myNFT, Method:POST
    def post(self, request, *args, **kwargs):
        owner = request.data['owner']

        # Check if any product exist
        try:
            queryset = ProductModel.objects.filter(owners__email=owner).filter(owners__nft_number__gte=1)
        except ProductModel.DoesNotExist:
            return Response(data={'message': 'No product registered'}, status=status.HTTP_204_NO_CONTENT)
        else:

            serializer = ProductSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)


# product type
def add_product_type(product, type):
    product.product_type.add(type)


# owner creator
def add_owner(owner):
    serializer = OwnerSerializer(data=owner)
    serializer.is_valid(raise_exception=True)
    serializer.save()

    return serializer.data['id']


# product type
def add_product_owner(product, owner, email):
    try:
        owner_data = {
            "email": email,
            "nft_number": product.nft_number
        }

        # add owner
        id = add_owner(owner_data)
        get_owner = OwnerModel.objects.get(id=id)

    except OwnerModel.DoesNotExist:
        return Response(data={'message': 'Owner does not exits'}, status=status.HTTP_204_NO_CONTENT)

    product.owners.add(get_owner)
    product.created_by.add(owner)
